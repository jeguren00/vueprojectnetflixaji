import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { authStore } from './store/authStore'
import { createPinia } from 'pinia'


const app = createApp(App)
app.use(router)
app.use(createPinia())

app.config.globalProperties.$auth = authStore();

app.mount('#app')
