import { defineStore } from 'pinia'

export const authStore = defineStore('auth', {
    state: () => {
        const auth = localStorage.getItem('auth');
        if (auth)
            return JSON.parse(auth);
        
        return {
            email: null,
            access_token: null,
            isAuthenticated: false
        }
    }, actions: {
        async login(email, password){
            console.log(email, password)
            const response = await fetch("http://netflixaji.herokuapp.com/api/login",
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email, password
                })
            });

            console.log(response)
            const data = await response.json();

            console.log(data)

            if (data.access_token) {
                this.email = email;
                this.password = password;
                this.access_token = data.access_token;
                this.isAuthenticated = true;

                localStorage.setItem('auth', JSON.stringify({ isAuthenticated: this.isAuthenticated, access_token: this.access_token, email: this.email }));
            }
        }
    }
})